﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;
using SlrrLib.View;

namespace CarPhysPositioning
{
  public partial class MainWindow : Window
  {
    private List<SlrrLib.Model.Cfg> wasChange = new List<SlrrLib.Model.Cfg>();
    private float tickForMove = 0.01f;
    private string lastOpenFileDialogDir = System.IO.Directory.GetCurrentDirectory();

    public MainWindow()
    {
      InitializeComponent();
      if (System.IO.File.Exists("lastDir"))
        lastOpenFileDialogDir = System.IO.File.ReadAllText("lastDir");
      SlrrLib.Model.MessageLog.SetConsoleLogOutput();
    }

    private void globalMoveBody(double deltaX, double deltaY, double deltaZ)
    {
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedCfgInducedModel>())
      {
        if (pivotObj == null)
          continue;
        if (pivotObj.BodyLine == null)
          continue;
        pivotObj.BodyLine.LineX += (float)deltaX;
        pivotObj.BodyLine.LineY += (float)deltaY;
        pivotObj.BodyLine.LineZ += (float)deltaZ;
        if (!wasChange.Any(x => x.CfgFileName == pivotObj.Cfg.CfgFileName))
          wasChange.Add(pivotObj.Cfg);
        ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(pivotObj);
      }
    }
    private void refreshModelGroups()
    {
      ctrlListOfModelGroups.Items.Clear();
      foreach (var modelGroup in ctrlOrbitingViewport.ModelGroups)
        ctrlListOfModelGroups.Items.Add(modelGroup);
    }
    private void refreshPivots()
    {
      ctrlListOfPivots.Items.Clear();
      foreach (var model in ctrlOrbitingViewport.CurrentModels)
        ctrlListOfPivots.Items.Add(model);
    }
    private SlrrLib.Geom.NamedModel getListBoxSelectedModel()
    {
      if (ctrlListOfPivots.SelectedItems.Count == 0)
        return null;
      return ctrlListOfPivots.SelectedItems[0] as SlrrLib.Geom.NamedModel;
    }

    private void ctrlButtonRenderScene_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
      {
        InitialDirectory = lastOpenFileDialogDir,
        FileName = "",
        DefaultExt = ".rpk",
        Filter = "Rpks (.rpk)|*.rpk"
      };

      var result = dlg.ShowDialog();

      string rpkFnam = "";
      if (result == true)
      {
        rpkFnam = dlg.FileName;
        lastOpenFileDialogDir = System.IO.Path.GetDirectoryName(rpkFnam);
      }
      else
      {
        return;
      }
      Title = rpkFnam;
      System.IO.File.WriteAllText("lastDir", lastOpenFileDialogDir);
      wasChange = new List<SlrrLib.Model.Cfg>();
      ctrlOrbitingViewport.RenderScene(new SlrrLib.Geom.RpkModelFactory(rpkFnam, SlrrLib.Model.HighLevel.GameFileManager.GetSLRRRoot(rpkFnam)));
      refreshPivots();
      refreshModelGroups();
    }
    private void ctrlListOfPivots_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      ctrlOrbitingViewport.SetAllModelsToTexture(@"grid_gray.png", 0.5, 0.5, Colors.DarkCyan);
      if (e.AddedItems.Count == 0)
        return;
      Vector3D avg = new Vector3D(0, 0, 0);
      double c = 0;
      var firstSel = e.AddedItems[0] as SlrrLib.Geom.NamedCfgInducedModel;
      if (firstSel != null && firstSel.BodyLine != null)
      {
        ctrlTextBoxBodyLinePosX.Text = UIUtil.FloatToString(firstSel.BodyLine.LineX);
        ctrlTextBoxBodyLinePosY.Text = UIUtil.FloatToString(firstSel.BodyLine.LineY);
        ctrlTextBoxBodyLinePosZ.Text = UIUtil.FloatToString(firstSel.BodyLine.LineZ);
      }
      foreach (var selItem in e.AddedItems)
      {
        var newlySelectedItem = selItem as SlrrLib.Geom.NamedModel;
        ctrlOrbitingViewport.SetModelToTexture(newlySelectedItem, @"grid.png", 1, 0.7, Colors.Red);
        c++;
        avg += newlySelectedItem.Translate;
      }
      ctrlOrbitingViewport.LookAtPosition(avg * (1.0 / c));
    }
    private void ctrlListOfModelGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (!ctrlListOfModelGroups.HasItems)
        return;
      if (e.AddedItems.Count == 0)
        ctrlOrbitingViewport.ChangeModelGroup(ctrlListOfModelGroups.Items[0] as string);
      else
        ctrlOrbitingViewport.ChangeModelGroup(e.AddedItems[0] as string);
      refreshPivots();
      ctrlListOfPivots.SelectedIndex = 0;
    }
    private void ctrlTextBoxBodyLinePosX_KeyUp(object sender, KeyEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      float parsed = 0;
      if (UIUtil.ParseOrFalse(ctrlTextBoxBodyLinePosX.Text, out parsed))
      {
        selModel.BodyLine.LineX = parsed;
        if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
          wasChange.Add(selModel.Cfg);
        ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
      }
    }
    private void ctrlTextBoxBodyLinePosY_KeyUp(object sender, KeyEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      float parsed = 0;
      if (UIUtil.ParseOrFalse(ctrlTextBoxBodyLinePosY.Text, out parsed))
      {
        selModel.BodyLine.LineY = parsed;
        if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
          wasChange.Add(selModel.Cfg);
        ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
      }
    }
    private void ctrlTextBoxBodyLinePosZ_KeyUp(object sender, KeyEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      float parsed = 0;
      if (UIUtil.ParseOrFalse(ctrlTextBoxBodyLinePosZ.Text, out parsed))
      {
        selModel.BodyLine.LineZ = parsed;
        if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
          wasChange.Add(selModel.Cfg);
        ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
      }
    }
    private void ctrlButtonSaveCfgChanges_Click(object sender, RoutedEventArgs e)
    {
      foreach (var cfg in wasChange)
      {
        cfg.Save();
      }
      wasChange.Clear();
    }
    private void ctrlTextBoxBodyLinePosX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      e.Handled = true;
      selModel.BodyLine.LineX += Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f);
      ctrlTextBoxBodyLinePosX.Text = UIUtil.FloatToString(selModel.BodyLine.LineX);
      if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
        wasChange.Add(selModel.Cfg);
      ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
    }
    private void ctrlTextBoxBodyLinePosY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      e.Handled = true;
      selModel.BodyLine.LineY += Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f);
      ctrlTextBoxBodyLinePosY.Text = UIUtil.FloatToString(selModel.BodyLine.LineY);
      if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
        wasChange.Add(selModel.Cfg);
      ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
    }
    private void ctrlTextBoxBodyLinePosZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      var selModel = getListBoxSelectedModel() as SlrrLib.Geom.NamedCfgInducedModel;
      if (selModel == null)
        return;
      if (selModel.BodyLine == null)
        return;
      e.Handled = true;
      selModel.BodyLine.LineZ += Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f);
      ctrlTextBoxBodyLinePosZ.Text = UIUtil.FloatToString(selModel.BodyLine.LineZ);
      if (!wasChange.Any(x => x.CfgFileName == selModel.Cfg.CfgFileName))
        wasChange.Add(selModel.Cfg);
      ctrlOrbitingViewport.UpdateTranslateOfModelFromPivot(selModel);
    }
    private void ctrlButtonGlobalBodyLinePosX_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      globalMoveBody(Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f), 0, 0);
      e.Handled = true;
    }
    private void ctrlButtonGlobalBodyLinePosY_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      globalMoveBody(0, Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f), 0);
      e.Handled = true;
    }
    private void ctrlButtonGlobalBodyLinePosZ_MouseWheel(object sender, MouseWheelEventArgs e)
    {
      globalMoveBody(0, 0, Math.Sign(e.Delta) * tickForMove * (Keyboard.IsKeyDown(Key.LeftShift) ? 10.0f : 1.0f));
      e.Handled = true;
    }
    private void ctrlButtonAlignPivots_Click(object sender, RoutedEventArgs e)
    {
      double bodyAvgX = 0;
      double bodyAvgY = 0;
      double bodyAvgZ = 0;
      double bodyCount = 0;
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedCfgInducedModel>())
      {
        if (pivotObj == null)
          continue;
        if (pivotObj.BodyLine == null)
          continue;
        bodyCount++;
        bodyAvgX += pivotObj.Translate.X + (pivotObj.BodyLine.LineX * 100.0);
        bodyAvgY += pivotObj.Translate.Y + (pivotObj.BodyLine.LineY * 100.0);
        bodyAvgZ += pivotObj.Translate.Z + (pivotObj.BodyLine.LineZ * 100.0);
      }
      bodyAvgX /= bodyCount;
      bodyAvgY /= bodyCount;
      bodyAvgZ /= bodyCount;
      double nonbodyAvgX = 0;
      double nonbodyAvgY = 0;
      double nonbodyAvgZ = 0;
      double nonbodyCount = 0;
      foreach (var pivotObj in ctrlOrbitingViewport.CurrentModels.OfType<SlrrLib.Geom.NamedScxModel>())
      {
        if (pivotObj == null)
          continue;
        if (pivotObj is SlrrLib.Geom.NamedCfgInducedModel)
          continue;
        nonbodyCount++;
        nonbodyAvgX += pivotObj.Translate.X;
        nonbodyAvgY += pivotObj.Translate.Y;
        nonbodyAvgZ += pivotObj.Translate.Z;
      }
      nonbodyAvgX /= nonbodyCount;
      nonbodyAvgY /= nonbodyCount;
      nonbodyAvgZ /= nonbodyCount;

      globalMoveBody(-(bodyAvgX - nonbodyAvgX) / 100.0, -(bodyAvgY - nonbodyAvgY) / 100.0, -(bodyAvgZ - nonbodyAvgZ) / 100.0);
    }
    private void ctrlButtonNextCFG_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlListOfModelGroups.SelectedIndex < ctrlListOfModelGroups.Items.Count - 1)
        ctrlListOfModelGroups.SelectedIndex++;
    }
    private void ctrlButtonPrevCFG_Click(object sender, RoutedEventArgs e)
    {
      if (ctrlListOfModelGroups.SelectedIndex > 0)
        ctrlListOfModelGroups.SelectedIndex--;
    }
  }
}
